Devuan Build Pipeline - how we build our packages.
==================================================

document is a Specification describing how the Devuan build infrastructure works for Devuan.


## Overview
-----------
The infrastructure consists of a pipeline that supports the creation
and management of packages and repos for the Devuan distribution. The
pipeline is based on a few components:

GDO: - git.devuan.org:
  a git repository with built in web interface - currently provided using a
  self hosted gitlab server

RELEASE_BOT - Devuan releasebot is a python program that sets up and manages 
jobs in the CI system.

CI: - ci.devuan.org, a continuous integration system that is used to build the
  packages  - currently provided by a Jenkins CI server

PDO: - packages.devuan.org, the master archive for all devuan built packages -
  currently runs dak (debian archive kit)


## Nomenclature
---------------

* ARCHIVE: this is where DEB (binary) packages and DSC (source) packages are 
  made available for use in the Devuan distrobution and derivatives.  Note:
  currently Devuan uses `amprolla` to provide a merged repository combining 
  packages from debian with those built for devuan in this build pipeline.

* DEB: the binary package that is produced by the Devuan build pipeline
  and made available via the ARCHIVE assembled by PDO.

* DISTRO: the distrobution - this will be regarded as "devuan" generally,
  but for the purposes of this document could be any distrobution 
  that is built in Devuan's build pipeline - it is restricted to all lowercase
  letters + numbers, eg `devuan`, `heads`, `n900`

* DSC: the source package built bu 

* GIT_REPO: the repository of the GIT PROJECTS, for all purposes in Devuan's 
  infrastructure we assume this to be `git.devuan.org`.

* PROJECT: is the name of a project as it appears in the REPO
  The name of a project will match the name of the source package in
  that it represents in the DISTRO: e.g. `surf2`.  The naming convention is 
  as per https://www.debian.org/doc/manuals/maint-guide/first.en.html#namever

* GIT_PROJECT: the name of the project with the suffix `.git` appended, eg
  `surf2.git`   
  This represents the compressed git archive and also the last part of the 
  GIT_URL.

* GIT_GROUP: the name of the group that the project resides in within the
  git repository.  Typically this represents a distinct group of packages making  up a distrobution, eg `devuan-packages` are where most packages specifically 
  built for devuan reside.  There is a special group in the REPO called `users` 
  which is for all git.devuan.org users private projects.
  GIT_GROUP appears as the first part of the path following the hostname in the
  GIT-URL

* GIT_URL: the complete URL of the git repo for a PROJECT, e.g.
  `https://git.devuan.org/devuan-packages/surf2.git`
  it represents the https access to the PROJECT and made up this way:
  `https://<REPO>/<GIT_GROUP/<GIT_PROJECT>`
  The GIT_URL is the used by ci.devuan.org to download the PROJECT source
  used to make the DEB 

* GIT_BRANCH: a development branch within the GIT_PROJECT.
  There are branch names that are reserved for exclusive use in this build
  pipeline as they denote the DISTRO and SUITE in the form `<DISTRO>/<SUITE>`
  Currently Devuan uses `suite` to represent the Devuan distrobution, so 
  `suite/jessie` represents the official source branch for Devuan's suite 
  `jessie`.
  
  See the git manual page for a more detailed explanation on git branches.
  (TODO: add a link to the relevant documentation section)

* GIT_ISSUE: an issue ticket in GDO

* GIT_PERMISSIONS: levels of privilege denoting granted permissions within a 
    GIT_PROJECT or GIT_GROUP.  Current levels are:
      `Owner` (Highest), `Master`, `Developer`, `Reporter`, `Guest` (Lowest)

    In addition there are 2 non-project related privilege Levels:
     - Administrator (effectively root or superuser of gitlab)
     - Anonomous (not logged in view only)
    
    Also, each GIT_PROJECT and GIT_GROUP has access controls:
     - Public = All users including Anonymous can view
     - Internal = All users logged in can view
     - Private = Only members can access

* SUITE: the section of a distro release, e.g.:
  `jessie`, `jessie-proposed`, `ascii-updates`
  (TODO: add reference to debian maint-guide documentation)

* PROJECT_BRANCH: a special GIT_BRANCH in a project branch identifying the files
  to be built for a specific DISTRO and SUITE, e.g.: `suites/jessie`,
  `maemo/ascii-proposed`, `heads/ceres`.  (Note that a PROJECT_BRANCH could
  be in a different GIT_GROUP then the one typically represents DISTRO,
  to allow for a maintainer to use a single PROJECT in the GIT_REPO to maintain
  packages for multiple distrobutions)

* CI_JOBS: Jenkins Jobs, these denote jobs pertaining to a project in 
  ci.devuan.org, usually they come in 3 jobs, SRC_JOB, BINARY_JOB and a
  REPO_JOB.

* SRC_JOB: this represents a job in ci.devuan.org that builds the devuan 
  source package.

* BINARY_JOB: this represents a job in ci.devuan.org that builds the devuan
  binary package.  
  
* REPO_JOB: this represents a job in ci.devuan.org that uploads the package
  into the REPO.



## How the Pipeline flows:
--------------------------

GDO - git.devuan.org 
  GDO is where the PROJECTS sources are stored.  It also currently provides
  the permissions model governing who can commit to the PROJECT_BRANCHES and 
  execute instructions to manage and build PROJECTS using the CI system.

RELEASE_BOT provides the executive glue between GDO and CI.  It polls GDO
  regularly (5 min cycle) looking for the ISSUEs assigned to the special user
  `Autobuild` with a title denoting the command that needs to be executed by 
  RELEASEBOT with respect to the CI system.

  These actions are:
    * `build`: Instructs CI to build the PROJECT's package with the LABEL 
      defining which PROJECT_BRANCH to build.  As a shortcut a LABEL
      with just the SUITE executes the BUILD for the `suite/<SUITE>`
      
      + Only members with `Master` or higher permissions in the GIT_PROJECT
        or GIT_GROUP containing that GIT_PROJECT can execute this action

    * `buildadd`: Creates the CI_JOBS for the PROJECT in the CI system
      + Only members with `Master` or higher PERMISSIONS in the special
        <DISTRO>-Masters GIT-PROJECT can execute this action.
        
    * `buildmodify`: Modifies the CI_JOBS for the PROJECT in the CI system.
      + Requires same permissions as for `buildadd`
     
    * `builddel`: Deletes the PROJECT's CI_JOBS from the CI system.
      + Requires same permissions as for `buildadd`


  Each Issue has a number of parameters specified in the form of LABELS.  The 
  types of accepted LABELS depends on the action type:
  
    * `build` actions accept the following LABELS:
      + any valid <SUITE>, eg `jessie`, `ascii-proposed` `jessie-security`
      (TODO: add pointer to documentation of current suites.

    * `buildadd` and `buildmodify` actions accept the following labels
      + `all` = architecture independent packages
      + `<arch>` = binary dependent architectures in the short form
          eg: `i386`, `amd64`, `armel`, `armhf`, `arm64`
        (TODO: add pointer to documentation of current supported architectures)
      + `<arch>_qemusys` = binary dependent architectures built using a full
        qemu system emulation of the architecture.  Currently available:
        `armel_qemusys`, `armhf_qemusys`, `arm64_qemusys`
        These are required for some packages where qemu user emulation is 
        insufficient - particular packages with heavy multi-threading and 
        low level hardware access during tests.
      + `any` = all architectures except `all`
      + `any_qemusys` = same as any but replacing all qemusys archs with their
        <arch>_qemusys variant
      + `sequential` = force building architectures sequentially instead of in
        parallel
      + `pbuilder_network` = allow the buildhost chroot to have internet access
      
      

## Continuous Integration service - Jenkins 

  The CI server at ci.devuan.org uses Jenkins to provide the coordination of 
  job execution - each job being handed off to an appropriate buildhost 
  for completion.

  For each PROJECT, a set of 3 CI-JOBS exist namely:
    * <PROJECT>-sources
    * <PROJECT>-binaries
    * <PROJECT>-repos

    These jobs are chained, so that succesful completion of PROJECT-sources job 
    triggers the execution of PROJECT-binaries, and successful completion of 
    that in turn triggers the execution of PROJECT-repos.

CI_JOB's build process in detail:

  * <PROJECT>-sources: this job clones the head commit of the PROJECT-BRANCH
    from GIT-URL. The PROJECT-BRANCH is determined by a LABEL in the ISSUE 
    RELEASE_BOT processes, and is obtained from the PROJECT-BRANCH from which the
  `build` SCORSH-CMD was issued. (This replaces the usage of Gitlab
  issue labels to identify the corect branch to be cloned). When it
  finishes, this job triggers [DISTRO:]PROJECT-binaries
  
* [DISTRO:]PROJECT-binaries: this job builds the package PKG using
  cow-builder/pbuilder, and creates all the .deb packages and the
  associated .dsc files. The parameters of the build are taken from
  the XML configuration snippet. When it finishes, this job triggers
  [DISTRO:]PROJECT-repos
  
* [DISTRO:]PROJECT-repos: this job is responsible for copying all the .deb
  files of PKG to the repo machine, and for triggering any action
  related to repo management, i.e. invoking `dak` or `reprepro` to
  update the repos.

### Job templates

Each DISTRO is associated to a set of XML Jenkins job templates, which
contain the configuration of the three Jenkins jobs associated to each
PKG. These XML templates can be customised to account for
DISTRO-specific needs, including the specific commands to be run in
order to update the corresponding repo. 
