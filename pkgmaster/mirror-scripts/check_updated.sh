#!/bin/sh

##
## Check that a mirror is updated by verifying that the Release and InRelease
## files are identical to those available under /home/vesta/devuan
##


if [ $# -lt 1 ]; then 
	echo "Usage: $0 <BaseURL> [<as-host>]"
	exit 1
fi


if  [ $# -gt 1 ]; then 
	AS_HOST="--header \"Host: $2\""
fi	

URL="$1"

LOCAL_REPO="/home/vesta/devuan"
REF_DIRS="devuan/dists merged/dists"

CHECK_FILES="Release Release.gpg InRelease"


for rd in ${REF_DIRS}; do 
	for d in $(ls ${LOCAL_REPO}/$rd); do	
		## avoid checks on symlinks
		if [ ! -h "${LOCAL_REPO}/$rd/$d" ]; then 
			for cf in ${CHECK_FILES}; do
				echo "Checking: $rd/$d/$cf"
				remote_sum=$(eval curl -s ${AS_HOST} ${URL}/$rd/$d/$cf | sha256sum | cut -d " " -f 1 )
				local_sum=$(cat ${LOCAL_REPO}/$rd/$d/$cf | sha256sum | cut -d " " -f 1)
				echo ${local_sum} ${remote_sum}
				diff=$(printf "${remote_sum}\n${local_sum}\n" | uniq | wc -l)
				if [ ! "$diff" = "1" ]; then
					ERRORS="${ERRORS} $rd/$d/$cf"
				fi
			done
		fi
	done 
done

[ -z "${ERRORS}" ] && exit 0

NUM_ERR=$(echo ${ERRORS}| sed -r -s 's/\ /\n/g' | wc -l)
exit ${NUM_ERR}

