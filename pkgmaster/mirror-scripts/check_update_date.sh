#!/bin/sh

##
## Check that a mirror is updated by looking at the Date reported in 
## Release files 
##


if [ $# -lt 1 ]; then 
	echo "Usage: $0 <BaseURL> [<as-host>]"
	exit 1
fi


if  [ $# -gt 1 ]; then 
	AS_HOST="--header \"Host: $2\""
fi	

URL="$1"

LOCAL_REPO="/home/vesta/devuan"
REF_DIRS="devuan/dists merged/dists"

CHECK_FILES="Release"


for rd in ${REF_DIRS}; do 
	for d in $(ls ${LOCAL_REPO}/$rd); do	
		## avoid checks on symlinks
		if [ ! -h "${LOCAL_REPO}/$rd/$d" ]; then 
			for cf in ${CHECK_FILES}; do
				echo "Checking: $rd/$d/$cf" 1>&2
				file_date=$(eval curl -s ${AS_HOST} ${URL}/$rd/$d/$cf | grep "^Date:" | cut -d " " -f 2-)
				file_time=$(date --date="${file_date}" +%Y%m%d%H%M%S)
				times="${file_time}|${file_date}\n${times}"
			done
		fi
	done 
done


oldest_time=$(echo ${times} | sort -rn | head -2 | tail -1 | cut -d "|" -f 2)

echo "oldest_time: ${oldest_time}" 1>&2

echo $(date --date="${oldest_time}" +%Y%m%d-%H:%M:%S)

