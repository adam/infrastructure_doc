= Info about some of the conf files used by debbugs =

- /etc/debbugs/config:
  This is the standard debbugs configuration file. It contains
  all the customisable variables which we need to set to have a 
  custom debbugs instance

- /etc/debbugs/Maintainers
  This is a list of emails of maintainers for each package/project. 
  The bug reports are forwarded to the corresponding maintainer. If 
  a maintainer email is not set for a bug, the bug report is not sent
  to any maintainer, but is still registered in the system

- /etc/debbugs/sources
  ???

- /etc/debbugs/nextnumber
  This is the next available bug number. Should not be reset....
